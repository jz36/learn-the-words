import React from "react";
import FooterColumn from "./FooterColumn";
import Paragraph from "../Paragraph";
import s from './Footer.module.scss'

const Footer = () => {
    return(
        <div className={s.footer}>
            <FooterColumn>
                <Paragraph>Первая колонка</Paragraph>
                <Paragraph>Ее информация</Paragraph>
            </FooterColumn>
            <FooterColumn>
                <Paragraph>Вторая колонка</Paragraph>
                <Paragraph>Ее информация</Paragraph>
            </FooterColumn>
            <FooterColumn>
                <Paragraph>Третья колонка</Paragraph>
                <Paragraph>Ее информация</Paragraph>
            </FooterColumn>
        </div>
    )
}

export default Footer;
