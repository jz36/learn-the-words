import React, {FunctionComponent} from "react";
import s from './FooterColumn.module.scss'


const FooterColumn : FunctionComponent = ({children}) => {
    return(
        <div className={s.footerColumn}>
            {children}
        </div>
    )
}

export default FooterColumn;
