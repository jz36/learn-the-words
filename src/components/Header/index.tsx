import React, {FunctionComponent} from 'react';
import s from './header.module.scss'

const Header: FunctionComponent = ({children}) => {
    return <h1 className={s.header}>{children}</h1>
}

export default Header;
