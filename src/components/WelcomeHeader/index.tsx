import React from "react";
import s from './WelcomeHeader.module.scss'

interface WelcomeHeaderProps {
    name: string,
    isNameDone: boolean,
    handleButtonClick: (event: React.MouseEvent<HTMLButtonElement>) => void,
    handleChangeValue: (event: React.ChangeEvent<HTMLInputElement>) => void,
}

class WelcomeHeader extends React.Component<WelcomeHeaderProps, any> {

    render(): React.ReactNode {
        const {handleButtonClick, handleChangeValue, name, isNameDone} = this.props;

        return (
            <div className={isNameDone? s.displayNone : s.cover}>
                <div className={isNameDone ? s.displayNone : s.welcomeWrap}>
                    <h1 className={s.welcomeHeader}>Привет!</h1>
                    <p className={s.welcomeParagraph}>Давай знакомиться</p>
                    <input onChange={handleChangeValue} className={s.welcomeInput} value={name} placeholder="Твое имя..."/>
                    <button onClick={handleButtonClick} className={s.welcomeButton}>OK</button>
                </div>
            </div>
        );
    }
}

export default WelcomeHeader;
