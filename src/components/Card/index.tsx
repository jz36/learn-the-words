import React from 'react';
import s from './Card.module.scss';

interface CardProps {
    eng: string,
    rus: string
}

class Card extends React.Component<CardProps, any>{

    state = {
        done: false,
    }
    handleCardClick = () => {
        const {done} = this.state;
        this.setState({done: !done})
    }

    render(): React.ReactNode {
        const {eng, rus} = this.props;
        const {done} = this.state

        const cardClass = [s.card];

        if (done){
            cardClass.push(s.done);
        } else if (cardClass.length > 1 && !done) {
            cardClass.pop()
        }

        return (
            <div
                onClick={this.handleCardClick}
                className={cardClass.join(' ')}>
                <div className={s.cardInner}>
                    <div className={s.cardFront}>
                        { eng }
                    </div>
                    <div className={s.cardBack}>
                        { rus }
                    </div>
                </div>

            </div>
        );
    }
}

export default Card;
