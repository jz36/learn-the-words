import React, {FunctionComponent} from 'react';
import s from './Paragraph.module.scss';

const Paragraph: FunctionComponent = ({children}) => {
    return <p className={s.descr}>{children}</p>
}

export default Paragraph;
