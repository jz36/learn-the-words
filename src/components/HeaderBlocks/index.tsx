import React, {FunctionComponent} from 'react';
import s from './headerBlock.module.scss'

interface HeaderBlockProps {
    hideBackground?: boolean,
}

const HeaderBlock: FunctionComponent<HeaderBlockProps> = ({hideBackground = false, children}) => {
    const styleCover = hideBackground ? {backgroundImage: 'none'} : {};
    return (
        <div className={s.cover} style={styleCover}>
            <div className={s.wrap}>
                {children}
            </div>
        </div>
    )
}

export default HeaderBlock;
