import React from 'react';
import HeaderBlock from "./components/HeaderBlocks";
import Header from "./components/Header";
import Paragraph from "./components/Paragraph";
import {wordsList} from "./Data/wordsList";
import Card from "./components/Card";
import Footer from "./components/Footer";
import WelcomeHeader from "./components/WelcomeHeader";


class App extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            name: "",
            isNameDone: false
        }
    }

    handleButtonClick = () => {
        if (this.state.name)
            this.setState({isNameDone: true})
    }

    handleChangeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({name: e.target.value})
    }

    render() {
        const {name, isNameDone} = this.state;
        return (
            <>
                <WelcomeHeader
                    name={name}
                    handleButtonClick={this.handleButtonClick}
                    handleChangeValue={this.handleChangeValue}
                    isNameDone={isNameDone}
                />
                <HeaderBlock>
                    <Header>Учите слова онлайн{name && isNameDone ? ", " + name + "!" : ""}</Header>
                    <Paragraph>Используйте карточки для запоминания и пополняйте активный запас</Paragraph>
                </HeaderBlock>
                <div>
                    {wordsList.map(({eng, rus}) => <Card eng={eng} rus={rus}/>)}
                </div>
                <HeaderBlock hideBackground>
                    <Header>Нам нравится это!</Header>
                    <Paragraph>Ну здорово же!</Paragraph>
                </HeaderBlock>
                <Footer/>
            </>
        )
    }
}

export default App;
